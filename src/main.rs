#![allow(dead_code)]
#![allow(unused_variables)]
#![allow(unused_imports)]

use std::fs::OpenOptions;
use std::io::{Error, Write};
use std::time::Instant;

mod selector;
use selector::*;
mod utils;
use utils::*;
mod general;
use general::*;
mod cross;
use cross::*;
mod mutation;
use mutation::*;
use std::fs;

const M: i32 = 5;
const N: i32 = 6;
const K: i32 = 24;
const POPULATION_SIZE: i32 = 1000;
const ITERATIONS: i32 = 100000;
const TURNIEJ_SIZE: usize = 5;
const CROSS_PROBABILITY: f64 = 0.5;
const MUTATION_PROBABILITY: f64 = 0.5;
const SELECTION_TYPE: &str = "turniej";

use plotters::prelude::*;

fn main() {
    algorytm_genetyczny();
    // algorytm_losowy();
}

fn algorytm_genetyczny() {
    let mut values_for_plot: Vec<(i32, i32, i32)> = Vec::new();

    let mut log_file = fs::OpenOptions::new()
        .append(true)
        .open("res/results.txt")
        .unwrap();

    let cst = read_constants(
        "res/flo_dane_v1.2/hard_cost.json",
        "res/flo_dane_v1.2/hard_flow.json",
    );

    let mut population: Vec<Vec<(i32, i32)>> = Vec::new();

    for _ in 0..POPULATION_SIZE {
        let os = random_osobnik(M, N, K);
        // println!("{:?}", os);
        population.push(os);
    }

    // Log
    {
        writeln!(&mut log_file).expect("Unable to write to the log file.");
        println!("Generated population of size {}", POPULATION_SIZE);
        writeln!(
            &mut log_file,
            "Generated population of size {}",
            POPULATION_SIZE
        )
        .expect("Unable to write to the log file.");
    }

    let results = costs_for_population(&cst, &population);

    // Log
    {
        println!("Best osobnik: {}", results.iter().min().unwrap());
        writeln!(
            &mut log_file,
            "Best osobnik: {}",
            results.iter().min().unwrap()
        )
        .expect("Unable to write to the log file.");
    }

    let now = Instant::now();

    // Algorytm genetyczny
    for i in 0..ITERATIONS {
        let mut new_population: Vec<Vec<(i32, i32)>> = Vec::new();
        let selector: Selector = S::new(&cst, &population);

        values_for_plot.push((
            selector.best_osobnik(),
            selector.average_osobnik(),
            selector.worst_osobnik(),
        ));

        while new_population.len() < population.len() {
            let mut new_osobnik = cross_equal(
                if SELECTION_TYPE.eq("ruletka") {
                    selector.ruletka_dwoch()
                } else {
                    selector.turniej_dwoch(TURNIEJ_SIZE)
                },
                CROSS_PROBABILITY,
            );

            mutate(M, N, &mut new_osobnik, MUTATION_PROBABILITY);
            
            new_population.push(new_osobnik);
        }

        population = new_population;
    }

    // Log
    {
        let time = now.elapsed();

        let results = costs_for_population(&cst, &population);

        println!("Best osobnik: {}", results.iter().min().unwrap());
        writeln!(
            &mut log_file,
            "Best osobnik: {}",
            results.iter().min().unwrap()
        )
        .expect("Unable to write to the log file.");
        println!("{} iterations done in {:?}", ITERATIONS, time);
        writeln!(
            &mut log_file,
            "{} iterations done in {:?}",
            ITERATIONS, time
        )
        .expect("Unable to write to the log file.");
    }

    let filename = format!(
        "images/graph_it{it}k_pop{pop}_c{c}_m{m}_{typ}{tsize}.png",
        it = ITERATIONS / 1000,
        pop = POPULATION_SIZE,
        c = CROSS_PROBABILITY,
        m = MUTATION_PROBABILITY,
        typ = SELECTION_TYPE,
        tsize = if SELECTION_TYPE.eq("turniej") {
            format!("_tsize{}", TURNIEJ_SIZE)
        } else {
            String::from("")
        }
    );

    println!("{}", &filename);

    plot(&values_for_plot, &filename);
}

fn algorytm_losowy() {
    let mut values_for_plot: Vec<(i32, i32, i32)> = Vec::new();

    let mut log_file = fs::OpenOptions::new()
        .append(true)
        .open("res/results.txt")
        .unwrap();

    let cst = read_constants(
        "res/flo_dane_v1.2/hard_cost.json",
        "res/flo_dane_v1.2/hard_flow.json",
    );

    let mut population: Vec<Vec<(i32, i32)>> = Vec::new();

    for _ in 0..POPULATION_SIZE {
        let os = random_osobnik(M, N, K);
        // println!("{:?}", os);
        population.push(os);
    }

    // Log
    {
        writeln!(&mut log_file).expect("Unable to write to the log file.");
        println!("Generated population of size {}", POPULATION_SIZE);
        writeln!(
            &mut log_file,
            "Generated population of size {}",
            POPULATION_SIZE
        )
        .expect("Unable to write to the log file.");
    }

    let results = costs_for_population(&cst, &population);

    // Log
    {
        println!("Best osobnik: {}", results.iter().min().unwrap());
        writeln!(
            &mut log_file,
            "Best osobnik: {}",
            results.iter().min().unwrap()
        )
        .expect("Unable to write to the log file.");
    }

    let now = Instant::now();

    let mut najlepszy_osobnik: i32 = 100000;

    // Algorytm genetyczny
    for i in 0..ITERATIONS {
        let mut new_population: Vec<Vec<(i32, i32)>> = Vec::new();
        let selector: Selector = S::new(&cst, &population);

        values_for_plot.push((
            najlepszy_osobnik,
            selector.average_osobnik(),
            selector.worst_osobnik(),
        ));

        if selector.best_osobnik() < najlepszy_osobnik {
            najlepszy_osobnik = selector.best_osobnik()
        }

        while new_population.len() < population.len() {
            new_population.push(random_osobnik(M, N, K));
        }

        population = new_population;
    }

    // Log
    {
        let time = now.elapsed();

        let results = costs_for_population(&cst, &population);

        println!("Best osobnik: {}", najlepszy_osobnik);
        writeln!(&mut log_file, "Best osobnik: {}", najlepszy_osobnik)
            .expect("Unable to write to the log file.");
        println!("{} iterations done in {:?}", ITERATIONS, time);
        writeln!(
            &mut log_file,
            "{} iterations done in {:?}",
            ITERATIONS, time
        )
        .expect("Unable to write to the log file.");
    }

    let filename = format!(
        "images/graph_it{it}k_pop{pop}_random.png",
        it = ITERATIONS / 1000,
        pop = POPULATION_SIZE
    );

    println!("{}", &filename);

    plot(&values_for_plot, &filename);
}

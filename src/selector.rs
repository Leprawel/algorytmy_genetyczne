use rand::distributions::Distribution;
use rand::Rng;
use statrs::distribution::Categorical;

use crate::general::*;

pub trait S<'a> {
    fn new(constants: &'a Vec<(Cost, Amount)>, population: &'a Vec<Vec<(i32, i32)>>) -> Self;
}

pub struct Selector<'a> {
    pub constants: &'a Vec<(Cost, Amount)>,
    pub population: &'a Vec<Vec<(i32, i32)>>,
    pub costs: Vec<i32>,
    pub categorical_distribution: Categorical,
}

impl<'a> S<'a> for Selector<'a> {
    fn new(constants: &'a Vec<(Cost, Amount)>, population: &'a Vec<Vec<(i32, i32)>>) -> Self {
        let costs = costs_for_population(&constants, &population);
        let max = (costs.iter().max().unwrap() + 1) as f64;
        let min = (costs.iter().min().unwrap() - 1) as f64;
        let distribution = Categorical::new(
            &costs
                .iter()
                .map(|val| (1.0 - (*val as f64 - min) / (max - min)))
                .collect::<Vec<f64>>(),
        )
        .unwrap();

        return Selector {
            constants: constants,
            population: population,
            costs: costs,
            categorical_distribution: distribution,
        };
    }
}

impl<'a> Selector<'a> {
    pub fn ruletka(&self) -> &Vec<(i32, i32)> {
        let pos = self
            .categorical_distribution
            .sample(&mut rand::thread_rng()) as usize;

        return &self.population[pos];
    }

    pub fn ruletka_dwoch(&self) -> (&Vec<(i32, i32)>, &Vec<(i32, i32)>) {
        return (self.ruletka(), self.ruletka());
    }

    pub fn turniej(&self, size: usize) -> &Vec<(i32, i32)> {
        let selection: Vec<usize> =
            vec![rand::thread_rng().gen_range(0..self.population.len()); size];

        let mut result = selection[0];

        for i in 1..selection.len() {
            if self.costs[i] < self.costs[result] {
                result = i;
            }
        }

        return &self.population[result];
    }

    pub fn turniej_dwoch(&self, size: usize) -> (&Vec<(i32, i32)>, &Vec<(i32, i32)>) {
        return (self.turniej(size), self.turniej(size));
    }

    pub fn best_osobnik(&self) -> i32{
        return *self.costs.iter().min().unwrap();
    }

    pub fn worst_osobnik(&self) -> i32{
        return *self.costs.iter().max().unwrap();
    }

    pub fn average_osobnik(&self) -> i32{
        let sum: i32 = Iterator::sum(self.costs.iter());
        return sum / (self.costs.len() as i32);
    }
}

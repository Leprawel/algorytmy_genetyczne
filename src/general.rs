use rand::Rng;
use serde::{Deserialize, Serialize};
use std::fs;

#[derive(Serialize, Deserialize, Debug)]
pub struct Cost {
    source: i32,
    dest: i32,
    cost: i32,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Amount {
    source: i32,
    dest: i32,
    amount: i32,
}

pub fn read_constants(filepath_cost: &str, filepath_amount: &str) -> Vec<(Cost, Amount)> {
    let file_cost = fs::read_to_string(filepath_cost).expect("Unable to read the file");
    let file_amount = fs::read_to_string(filepath_amount).expect("Unable to read the file");

    let cost: Vec<Cost> = serde_json::from_str(&file_cost).expect("Unable to convert to json");
    let amount: Vec<Amount> =
        serde_json::from_str(&file_amount).expect("Unable to convert to json");

    return std::iter::zip(cost, amount).collect();
}

pub fn random_osobnik(m: i32, n: i32, k: i32) -> Vec<(i32, i32)> {
    let mut osobnik: Vec<(i32, i32)> = Vec::new();
    let mut r = rand::thread_rng();
    for _ in 0..k {
        let mut proposition = (r.gen_range(0..m), r.gen_range(0..n));
        while osobnik.iter().any(|&p| p == proposition) {
            proposition = (r.gen_range(0..m), r.gen_range(0..n));
        }
        osobnik.push(proposition);
    }

    return osobnik;
}

pub fn move_machine(num: usize, pos: (i32, i32), os: &mut Vec<(i32, i32)>) {
    // println!("Moving {} to {:?}", num, pos);

    if let Some(dest) = os.iter().position(|&x| x == pos) {
        // println!("Swapping with {}", dest);
        os[dest] = os[num];
    }

    os[num] = pos;
}

pub fn cost(constants: &Vec<(Cost, Amount)>, osobnik: &Vec<(i32, i32)>) -> i32 {
    let mut result = 0;

    for c in constants {
        let distance = m_dist(&osobnik[c.0.source as usize], &osobnik[c.0.dest as usize]);
        result += distance * c.0.cost * c.1.amount;
    }

    return result;
}

pub fn costs_for_population(
    constants: &Vec<(Cost, Amount)>,
    population: &Vec<Vec<(i32, i32)>>,
) -> Vec<i32> {
    let mut costs: Vec<i32> = Vec::new();
    for os in population {
        costs.push(cost(constants, os));
    }
    return costs;
}

fn m_dist(pos1: &(i32, i32), pos2: &(i32, i32)) -> i32 {
    return (pos1.0 - pos2.0).abs() + (pos1.1 - pos2.1).abs();
}

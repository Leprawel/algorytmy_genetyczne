use crate::general::*;
use rand::Rng;

pub fn mutate(m: i32, n: i32, target: &mut Vec<(i32, i32)>, p: f64) {
    let mut r = rand::thread_rng();

    if r.gen::<f64>() < p {
        for i in 0..target.len() {
            if r.gen::<f64>() < (1.0 / target.len() as f64) {
                move_machine(i, (r.gen_range(0..m), r.gen_range(0..n)), target);
            }
        }
    }
}

mod selector;
mod utils;
mod general;
mod cross;
mod mutation;

#[cfg(test)]
mod tests {

    #[test]
    fn turniej_z_calosci() {
        use crate::selector::*;
        use crate::general::*;

        const M: i32 = 4;
        const N: i32 = 3;
        const K: i32 = 9;
        const POPULATION_SIZE: i32 = 5;

        let cst = read_constants(
            "res/flo_dane_v1.2/easy_cost.json",
            "res/flo_dane_v1.2/easy_flow.json",
        );
    
        let mut population: Vec<Vec<(i32, i32)>> = Vec::new();
    
        for _ in 0..POPULATION_SIZE {
            let os = random_osobnik(M, N, K);
            println!("{:?}", &os);
            println!("{:?}", cost(&cst, &os));
            population.push(os);
        }
        
        let selector: Selector = S::new(&cst, &population);

        let s = selector.turniej(POPULATION_SIZE as usize);
        
        println!("Osobnik: {:?}", &s);
        println!("Osobnik: {:?}", &population[selector.costs.iter().enumerate().min_by_key(|(_, &value)| value).unwrap().0]);

        assert_eq!(s, &population[selector.costs.iter().enumerate().min_by_key(|(_, &value)| value).unwrap().0]);
    }

    
}
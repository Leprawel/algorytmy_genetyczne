use plotters::prelude::*;
use serde::de::value;

pub fn print_osobnik(m: i32, n: i32, osobnik: &Vec<(i32, i32)>) {
    for y in 0..n {
        for x in 0..m {
            if let Some(pos) = osobnik.iter().position(|&p| p == (x, y)) {
                print!("{}\t", pos);
            } else {
                print!("|\t");
            }
        }
        println!();
    }
}

pub fn plot(values: &Vec<(i32, i32, i32)>, filename: &str) -> () {
    let root_area = BitMapBackend::new(filename, (1920, 1080)).into_drawing_area();
    root_area.fill(&RGBColor(200, 200, 200)).unwrap();

    let mut plt = ChartBuilder::on(&root_area)
        .x_label_area_size(50)
        .y_label_area_size(100)
        .right_y_label_area_size(100)
        .margin_top(50)
        .build_cartesian_2d(
            0..values.len() as i32,
            (-500 + values.iter().min_by(|a, b| a.0.cmp(&b.0)).unwrap().0 as i32)
                ..(values.iter().max_by(|a, b| a.0.cmp(&b.0)).unwrap().0 as i32),
        )
        .unwrap();

    plt.configure_mesh()
        .x_label_style(TextStyle::from(("sans-serif", 32).into_font()))
        .y_label_style(TextStyle::from(("sans-serif", 32).into_font()))
        .draw()
        .unwrap();

    plt.draw_series(LineSeries::new(
        values.iter().enumerate().map(|(i, val)| (i as i32, val.2)),
        &BLUE,
    ))
    .unwrap();

    plt.draw_series(LineSeries::new(
        values.iter().enumerate().map(|(i, val)| (i as i32, val.1)),
        &RED,
    ))
    .unwrap();

    plt.draw_series(
        LineSeries::new(
            values.iter().enumerate().map(|(i, val)| (i as i32, val.0)),
            &GREEN,
        )
        .point_size(3u32),
    )
    .unwrap();
}

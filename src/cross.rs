use crate::general::*;
use rand::Rng;

pub fn cross_equal(parents: (&Vec<(i32, i32)>, &Vec<(i32, i32)>), p: f64) -> Vec<(i32, i32)> {
    let mut r = rand::thread_rng();
    let mut new_os: Vec<(i32, i32)> = parents.0.clone();

    if r.gen::<f64>() < p{
        for (i, &item) in parents.1.iter().enumerate() {
            if item == new_os[i] {
                continue;
            };
            if r.gen::<f64>() < (1.0/parents.1.len() as f64) {
                move_machine(i, item, &mut new_os);
            }
        }
    }

    return new_os;
}
